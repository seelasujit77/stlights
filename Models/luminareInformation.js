
module.exports = luminareInformation;


function luminareInformation(make, modelNumber, dateOfPurchase,vendorName,warrantyExpiryDate,luminaireType,nominalVoltage,maximumInputCurrent) {
    // custom type checking here...
    this.make =     make;
    this.modelNumber =   modelNumber;
    this.dateOfPurchase = dateOfPurchase;
    this.vendorName = vendorName;
    this.warrantyExpiryDate = warrantyExpiryDate;
    this.luminaireType = luminaireType;
    this.nominalVoltage = nominalVoltage;
    this.maximumInputCurrent = maximumInputCurrent;
    this.luminareImage = [];
  }



  function savedImagesId(imagesArray,mongodb) {
    var ret;

        mongodb.insertImage('userDetails',imagesArray,function(err,result){

                        if(err)  return null;
                        else{
                          array = result;
                          ret= result;
                        }
                      });
                      while(ret === undefined) {
                        require('deasync').runLoopOnce();
                      }

                      return ret;  

  }

  luminareInformation.prototype.addImages = function(array,mongodb) {

    
  var details = savedImagesId(array,mongodb)

  for(var i = 0 ;i<array.length;i++)
    {
      var obj ={
        imagePath:array[i].imagePath,
        image:details[i]
      };

      this.luminareImage.push(obj);
    }
 
  
};

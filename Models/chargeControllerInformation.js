
module.exports = chargeControllerInformation;

  function chargeControllerInformation(make, modelNumber, dateOfPurchase,vendorName,warrantyExpiryDate,chargeControllerType,nominalVoltage,maximumInputCurrent,maximumLoadCurrent) {
    // custom type checking here...
    this.make =     make;
    this.modelNumber =   modelNumber;
    this.dateOfPurchase = dateOfPurchase;
    this.vendorName = vendorName;
    this.warrantyExpiryDate = warrantyExpiryDate;
    this.chargeControllerType = chargeControllerType;
    this.nominalVoltage = nominalVoltage;
    this.maximumInputCurrent = maximumInputCurrent;
    this.maximumLoadCurrent=maximumLoadCurrent;
    this.chargerControllerImage =[];
  }



  function savedImagesId(imagesArray,mongodb) {
    var ret;

        mongodb.insertImage('userDetails',imagesArray,function(err,result){

                        if(err)  return null;
                        else{
                          array = result;
                          ret= result;
                        }
                      });
                      while(ret === undefined) {
                        require('deasync').runLoopOnce();
                      }

                      return ret;  

  }

  chargeControllerInformation.prototype.addImages = function(array,mongodb) {

    
  var details = savedImagesId(array,mongodb)

  for(var i = 0 ;i<array.length;i++)
    {
      var obj ={
        imagePath:array[i].imagePath,
        image:details[i]
      };

      this.chargerControllerImage.push(obj);
    }
 
  
};

module.exports = generalInformation;


function generalInformation(installationId, projectName,installerName,mobileNumber,installationDate,latitude,longitude) {
    // custom type checking here...
    this.installationId =     installationId;
    this.projectName =  projectName;
    this.installerName =     installerName;
    this.mobileNumber =  mobileNumber;
    this.installationDate =     installationDate;
    this.latitude =  latitude;
    this.longitude =     longitude;
  }


module.exports = batteryInformation;

function batteryInformation(projectMake, modelNumber, dateOfPurchase,batteryType,nominalVoltage,capacity,minimumChargingVoltage,maximumChargingVoltage) {
    // custom type checking here...
    this.projectMake =     projectMake;
    this.modelNumber =   modelNumber;
    this.dateOfPurchase = dateOfPurchase;
    this.batteryType = batteryType;
    this.nominalVoltage = nominalVoltage;
    this.capacity = capacity;
    this.minimumChargingVoltage = minimumChargingVoltage;
    this.maximumChargingVoltage = maximumChargingVoltage;
    this.batteryImage = [];
    
  }




  function savedImagesId(imagesArray,mongodb) {
    var ret;

        mongodb.insertImage('userDetails',imagesArray,function(err,result){

                        if(err)  return null;
                        else{
                          array = result;
                          ret= result;
                        }
                      });
                      while(ret === undefined) {
                        require('deasync').runLoopOnce();
                      }

                      return ret;  

  }

  batteryInformation.prototype.addImages = function(array,mongodb) {

    
  var details = savedImagesId(array,mongodb)

  for(var i = 0 ;i<array.length;i++)
    {
      var obj ={
        imagePath:array[i].imagePath,
        image:details[i]
      };

      this.batteryImage.push(obj);
    }
 
  
  
};



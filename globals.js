var Path = require('path');

function Globals() {
};

//DB configurations
Globals.prototype.MongoPort = 27017;
//Current production server
Globals.prototype.MongoHost = 'localhost';
Globals.prototype.MongoDB = 'ITC';
Globals.prototype.appRoot = Path.resolve(__dirname);
Globals.prototype.graceTime = 30000; // in milliseconds used for hub on off status schedule time

Globals.prototype.LoggerConfig = {
    appenders: [
        {type: 'console'},
        {
            "type": "file",
            "filename": Globals.prototype.appRoot + "/logs/log_file.log",
            "maxLogSize": 10485760,
            "backups": 100,
            "category": "relative-logger"
        }
    ]
};


module.exports = new Globals();

var express    = require('express'),
    app        = express(),
    mongodb = require('./db'),
    bodyParser = require('body-parser'),
    port = process.env.PORT ||  8080,
     _ =require('underscore'),      
 Buffer = require('buffer/').Buffer,
//    port = process.env.PORT ||  3000,
    Globals = require('./globals'),
    log4js = require('log4js');

var multer=require("multer");
var upload = multer({ dest: './tmp/'});
var async = require('async');
var Server = require('mongodb').Server,
    ReplSetServers = require('mongodb').ReplSetServers,
    ObjectID = require('mongodb').ObjectID,
    Binary = require('mongodb').Binary,
    GridStore = mongodb.GridStore,
    Code = require('mongodb').Code,
    BSON = require('mongodb-core').BSON,
    assert = require('assert');
    fs = require('fs'),
    Grid = require('gridfs-stream');


var formidable = require('formidable');

var DeviceDetails = require('./DeviceDetails');

log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');
var fs = require('fs');

app.use(function(req,res,next){
  
  res.header('Access-Control-Allow-Origin', '*'); // We can access from anywhere
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
    next();
});





app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({   uploadDir: app.settings.tmpFolder,extended: true }));
app.use(bodyParser.json());
app.use(logResponseBody);




function logResponseBody(req, res, next) {
    var oldWrite = res.write,
        oldEnd = res.end,
        chunks = [],
        t1 = new Date();  

    res.write = function (chunk) {
        chunks.push(chunk);
        oldWrite.apply(res, arguments);
    };

    res.end = function (chunk) {
        if (chunk)
            chunks.push(chunk);
        var t2 = new Date();
        
        oldEnd.apply(res, arguments);
    };

    next();
};


process.on('SIGINT', function() {
    mongodb.close(function(){
        logger.info('closing db');
        process.exit(0);
    });
});

process.on('uncaughtException', function(err) {
    // handle the error safely
    logger.info(err.stack);
});



mongodb.connect(Globals.MongoHost, Globals.MongoPort, Globals.MongoDB, function(err){

if (err) {

  logger.info("Problem in connecting to MongoDB."+err);
}
else
{
  logger.info("Connected to MongoDB");
  app.listen(port,function(){
    logger.info('API\'s work at http://localhost:' +port+ "url.");
  });
}


});

app.get('/', function(req, res) {
    res.sendFile(Globals.appRoot + '/public/form.html');
});


//////////////////////////////////show all device details//////////////////////////////////

app.get('/allorganizations',function(req,res){
     mongodb.findAll("userDetails",function(err,result){

        if(err)
         {
             res.send({"status":0,"message":"error in retriving the record"});
         }
         else{

             res.json(result); 

         }

     });
   });


   ////////////////////////////////// Add device details//////////////////////////////


app.post('/addorganization',upload.any(),function(req,res){

 var mongodbs = require('mongodb');

var query =
{
"generalInformations.installationId":req.body.generalInformation.installationId
};
//  mongodb.findByObjects("userDetails",query,function(err,deviceResult){
    
//         if(err)
//             {
//                 res.send({"status":0,"message":"error in retriving the record"});
                
//             }
//else
          //      {
   // if(deviceResult.length>0)
    //    {
        //    res.send({"status":0,"message":"This organization is already registered with us Please register with another organization"});
            
     //   }
      //  else
       //     {


                var parsedResponse = JSON.parse(JSON.stringify(req.body));
    

                var bodyDetails = new DeviceDetails(parsedResponse,mongodb);
                    // mongodb.insertImage('userDetails',req,function(err,result){

                    //     if(err)  res.send({"status":0,"message":"error in insertion of the record"});
                    //     else{

           
                          mongodb.save('userDetails',bodyDetails,function(err,resultss){
                            
                                 if (err) {
                                         res.send({"status":0,"message":"error in insertion of the record"});
                                  }
                                  else
                                  {
                                    res.json(resultss);
                            
                                  }
                            
                               });

                       //     }
                  //   });

          //  }

           //     }
    
    
     //    });


});


///////////////////////// Update device details ///////////////////////////

// app.post('/updateUser/:deviceID',upload.any(),function(req,res){

    
//     var updateFrom = { deviceID: req.params.deviceID};
//     var updateTo = {$set: {deviceName:req.body.deviceName,deviceSNO:req.body.deviceSNO, deviceType:req.body.deviceType}};
    
//     mongodb.updateByObjects("userDetails",updateFrom,updateTo,function(err,deviceResult){
        
//         if(err)
//                 {
//                     res.send({"status":0,"message":"error in retriving the record"});
                    
//                 }
//                 else
//                     {
//         if(deviceResult)
//             {
//                 res.send({"status":0,"message":"Updated Successfully"});
//             }
//             else
//                 {
//                     res.send({"status":0,"message":"Device ID not found"});
                    
//                 }
//             }

//   });

// });


////////////////////////// Get device details//////////////////////////

app.get('/organization/:installationId',function(req,res){

    if (req.params.installationId)
        {
            var query =
            {
            "generalInformations.installationId":req.params.installationId
            };

 mongodb.findByObjects("userDetails",query,function(err,results){
    
        if(err)
            {
                res.send({"status":0,"message":"error in retriving the record"});
                
            }
            else
                { 
                    if(results.length>0)
                     {
                        res.json(results);
                        
                        }
                     else
                         {
                            res.send({"status":0,"message":"No devices found with this ID"});
                            
                          }
            }


     });

        }
        else
            {
                res.send({"status":0,"message":"Please send us the ID which you want to retrive from."});
                
            }

});

//////////////////show and display DeviceImage//////////////////////////////



app.get('/image/:_id',function(req,res){


logger.info('id information'+req.params._id);
if (req.params._id)
 {
 var mongodbs = require('mongodb');
 var pi_id   = new ObjectID(req.params._id);

mongodb.dbDetails.collection('usersImage.files')
  .find({_id: pi_id})
  .toArray(function(err, files) {
    if (err) throw err;
    files.forEach(function(file) {
        if (file) {    
 var gfs = Grid(mongodb,mongodbs);

   var bucket = new mongodbs.GridFSBucket(mongodb.dbDetails, {
            chunkSizeBytes: 1024,
            bucketName: 'usersImage'
        });

   var readStream = bucket.openDownloadStream(pi_id); // The readable stream

res.once("finish", function (data) {

res.end(data);
});

res.on("error", function (err) {
next(err);
});


var path = require('path')
var extname = path.extname(file.filename).substring(1);
console.log('image/'+extname);
res.header("content-type", 'image/'+extname);
readStream.pipe(res);


  }
  else{
    console.log('asdfasdfasdfsdf');
  }
    });
  });


  }
  else
  {
     res.send({"status":0,"message":"Please give the image ID"});
  }

});


////////////////////////Delete and device from mongo//////////////////////////////

// app.delete('/deletedevice/:id',function(req,res){
//     var id=req.params._id;
  
//     mongodb.deleteManyByObject("userDetails",{deviceID:id},function(err,result){

//         if(err)
//          {
//              res.send({"status":0,"message":"unable to delete this record"});
//          }
//          else{   
//              res.send({"status":0,"message":"Successfully deleted this object"});
//          }

//      });
// });

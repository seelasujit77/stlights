// function generalInformation(installationId, projectName,installerName,mobileNumber,installationDate,latitude,longitude) {
//     // custom type checking here...
//     this.installationId =     installationId;
//     this.projectName =  projectName;
//     this.installerName =     installerName;
//     this.mobileNumber =  mobileNumber;
//     this.installationDate =     installationDate;
//     this.latitude =  latitude;
//     this.longitude =     longitude;
//   }


//   ///////////////////////////////////////////      panelInformation   ////////////////////////////////////////////////


//   function panelInformation(projectMake, modelNumber, dateOfPurchase,warrantyExpiryDate,maxRatedPower,panelImage) {
//     // custom type checking here...
//     this.projectMake =     projectMake;
//     this.modelNumber =   modelNumber;
//     this.dateOfPurchase = dateOfPurchase;
//     this.warrantyExpiryDate = warrantyExpiryDate;
//     this.maxRatedPower = maxRatedPower;
//     this.panelImage = panelImage;
    
    
//   }

//   function panelImage(path,imageName)
//   {
//     this.path =     path;
//     this.imageName =   imageName;

//   }
  

//     ///////////////////////////////////////////      batteryInformation   ////////////////////////////////////////////////

//     function batteryInformation(projectMake, modelNumber, dateOfPurchase,batteryType,nominalVoltage,capacity,minimumChargingVoltage,maximumChargingVoltage) {
//         // custom type checking here...
//         this.projectMake =     projectMake;
//         this.modelNumber =   modelNumber;
//         this.dateOfPurchase = dateOfPurchase;
//         this.batteryType = batteryType;
//         this.nominalVoltage = nominalVoltage;
//         this.capacity = capacity;
//         this.minimumChargingVoltage = minimumChargingVoltage;
//         this.maximumChargingVoltage = maximumChargingVoltage;
//       }



//         ///////////////////////////////////////////      chargeControllerInformation   ////////////////////////////////////////////////

//     function chargeControllerInformation(make, modelNumber, dateOfPurchase,vendorName,warrantyExpiryDate,chargeControllerType,nominalVoltage,maximumInputCurrent,maximumLoadCurrent) {
//         // custom type checking here...
//         this.make =     make;
//         this.modelNumber =   modelNumber;
//         this.dateOfPurchase = dateOfPurchase;
//         this.vendorName = vendorName;
//         this.warrantyExpiryDate = warrantyExpiryDate;
//         this.chargeControllerType = chargeControllerType;
//         this.nominalVoltage = nominalVoltage;
//         this.maximumInputCurrent = maximumInputCurrent;
//         this.maximumLoadCurrent=maximumLoadCurrent;
//       }



//         ///////////////////////////////////////////      luminareInformation   ////////////////////////////////////////////////

//     function luminareInformation(make, modelNumber, dateOfPurchase,vendorName,warrantyExpiryDate,luminaireType,nominalVoltage,maximumInputCurrent) {
//         // custom type checking here...
//         this.make =     make;
//         this.modelNumber =   modelNumber;
//         this.dateOfPurchase = dateOfPurchase;
//         this.vendorName = vendorName;
//         this.warrantyExpiryDate = warrantyExpiryDate;
//         this.luminaireType = luminaireType;
//         this.nominalVoltage = nominalVoltage;
//         this.maximumInputCurrent = maximumInputCurrent;
//       }


var generalInformation = require('./Models/generalInformation');
var panelInformation = require('./Models/panelInformation');
var batteryInformation = require('./Models/batteryInformation');
var chargeControllerInformation = require('./Models/chargeControllerInformation');
var luminareInformation = require('./Models/luminareInformation');




      function deviceDetail(generalInformation,panelInformation,batteryInformation,chargeControllerInformation,luminareInformation)
     {
this.generalInformation = generalInformation;
this.panelInformation = panelInformation;
this.batteryInformation = batteryInformation;
this.chargeControllerInformation = chargeControllerInformation;
this.luminareInformation = luminareInformation;
      }


      function DeviceDetails(details,mongodb)
      {
        var infoOfGeneralInformation = new generalInformation(details.generalInformation.installationId, details.generalInformation.projectName,details.generalInformation.installerName,details.generalInformation.mobileNumber,details.generalInformation.installationDate,details.generalInformation.latitude,details.generalInformation.longitude);
        
          var infoOfPanelInformation = new panelInformation(details.panelInformation.projectMake, details.panelInformation.modelNumber, details.panelInformation.dateOfPurchase,details.panelInformation.warrantyExpiryDate,details.panelInformation.maxRatedPower,details.panelInformation.panelImage);
          if(details.panelInformation.panelImage){
          infoOfPanelInformation.addImages(details.panelInformation.panelImage,mongodb);
          }
        
          var infoOfBatteryInformation = new batteryInformation(details.batteryInformation.projectMake, details.batteryInformation.modelNumber, details.batteryInformation.dateOfPurchase,details.batteryInformation.batteryType,details.batteryInformation.nominalVoltage,details.batteryInformation.capacity,details.batteryInformation.minimumChargingVoltage,details.batteryInformation.maximumChargingVoltage);
          
          
          if(details.batteryInformation.batteryImage){
          infoOfBatteryInformation.addImages(details.batteryInformation.batteryImage,mongodb);
          }


       
          var infoOfChargeControllerInformation =   new chargeControllerInformation(details.chargeControllerInformation.make, details.chargeControllerInformation.modelNumber, details.chargeControllerInformation.dateOfPurchase,details.chargeControllerInformation.vendorName,details.chargeControllerInformation.warrantyExpiryDate,details.chargeControllerInformation.chargeControllerType,details.chargeControllerInformation.nominalVoltage,details.chargeControllerInformation.maximumInputCurrent,details.chargeControllerInformation.maximumLoadCurrent);
        
          if(details.chargeControllerInformation.chargerControllerImage){
            infoOfChargeControllerInformation.addImages(details.chargeControllerInformation.chargerControllerImage,mongodb);
            
          }

          
        var infoOfLuminareInformation = new luminareInformation(details.luminareInformation.make, details.luminareInformation.modelNumber, details.luminareInformation.dateOfPurchase,details.luminareInformation.vendorName,details.luminareInformation.warrantyExpiryDate,details.luminareInformation.luminaireType,details.luminareInformation.nominalVoltage,details.luminareInformation.maximumInputCurrent);
      
        if(details.luminareInformation.luminareImage){
          infoOfLuminareInformation.addImages(details.luminareInformation.luminareImage,mongodb);
          
        }

return new deviceDetail(infoOfGeneralInformation,infoOfPanelInformation,infoOfBatteryInformation,infoOfChargeControllerInformation,infoOfLuminareInformation);

      }

      module.exports = DeviceDetails;
      